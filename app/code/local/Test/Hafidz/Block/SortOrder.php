<?php

class Test_Hafidz_Block_SortOrder extends Mage_Core_Block_Template
{
    private $sortAsc = 'asc';
    private $sortDesc = 'desc';

    /**
     * @return string
     */
    public function getUrlSortAsc()
    {
        return $this->getUrl('test/productfillter/',['sort' => $this->sortAsc]);
    }

    /**
     * @return string
     */
    public function getUrlSortDesc()
    {
        return $this->getUrl('test/productfillter/',['sort' => $this->sortDesc]);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isCurrentSortDesc()
    {
        $sortParam = $this->getRequest()->getParam('sort');
        if($sortParam === $this->sortDesc){
            return false;
        }

        return true;
    }

    /**
     * @return Mage_Catalog_Model_Product
     * @throws Exception
     */
    public function getProducts(){
        $sortParam = $this->getRequest()->getParam('sort') ?: $this->sortAsc;

        /** @var Mage_Catalog_Model_Product $products */
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter('sort_order', array('notnull' => true))
            ->setOrder('sort_order', $sortParam)
        ;

        return $products;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getProductPrice(Mage_Catalog_Model_Product $product){
        return Mage::helper('core')->currency($product->getPrice(),true,false);
    }
}