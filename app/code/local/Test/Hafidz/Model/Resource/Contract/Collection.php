<?php
/**
 * Created by PhpStorm.
 * User: muhammadhafidzistihori
 * Date: 2019-03-27
 * Time: 12:11
 */ 
class Test_Hafidz_Model_Resource_Contract_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('test_hafidz/contract');
    }

}